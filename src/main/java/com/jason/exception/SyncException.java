package com.jason.exception;

/**
 * @author he peng
 * @create 2017/12/18 11:15
 * @see
 */
public class SyncException extends RuntimeException {

    private Integer errorCode;

    public SyncException() {}

    public SyncException(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public SyncException(String message, Integer errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public SyncException(String message, Throwable cause, Integer errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public SyncException(Throwable cause, Integer errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public SyncException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Integer errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public SyncException setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    @Override
    public String toString() {
        return "SyncException{" +
                "errorCode=" + errorCode +
                '}';
    }
}
