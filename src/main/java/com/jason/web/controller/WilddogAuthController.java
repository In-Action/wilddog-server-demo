package com.jason.web.controller;

import com.jason.core.wilddog.service.WilddogAuthService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author he peng
 * @create 2017/12/13 13:34
 * @see
 */

@RestController
@RequestMapping(
        path = "/wilddog"
)
public class WilddogAuthController {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogAuthController.class);

    @Autowired
    private WilddogAuthService wilddogAuthService;

    @GetMapping(
            path = "/auth"
    )
    public ResponseEntity<String> getCustomToken() {
        if (LOG.isInfoEnabled()) {
            LOG.info("get Custom Token for Wilddog Auth.");
        }
        Map<String, Object> claims = new HashMap<>(4);
        claims.put("claim_0" , DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss"));
        claims.put("claim_1" , 1234556);
        return ResponseEntity.ok(this.wilddogAuthService.generateCustomToken(UUID.randomUUID().toString(), claims));
    }
}
