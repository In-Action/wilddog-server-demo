package com.jason.web.controller;

import com.jason.core.wilddog.listener.CompletionListenerLogger;
import com.jason.core.wilddog.node.SurveyNode;
import com.jason.core.wilddog.service.WilddogAuthService;
import com.jason.core.wilddog.util.Base64Utils;
import com.jason.core.wilddog.util.WilddogSyncUtils;
import com.jason.dto.PicUploadDTO;
import com.jason.dto.VideoConnectionDTO;
import net.sf.jmimemagic.Magic;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author he peng
 * @create 2017/12/13 12:13
 * @see
 */

@RestController
public class WilddogDemoController {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogDemoController.class);

    @Autowired
    private WilddogAuthService wilddogAuthService;

    @GetMapping(
            path = "/test" ,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<String> test() {
        if (LOG.isInfoEnabled()) {
            LOG.info("request /test ...");
        }
        return ResponseEntity.ok(DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss"));
    }

    @PostMapping(
            path = "/upload" ,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE ,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<Map<String , Object>> upload(@RequestBody PicUploadDTO picUpload) throws Exception {


        byte[] bytes = Base64Utils.decode(picUpload.getContent());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        String mimeType = Magic.getMagicMatch(bytes).getExtension();
        String fileName = UUID.randomUUID().toString() + "." + mimeType;
        FileOutputStream fileOutputStream = new FileOutputStream("D:/Documents/Pictures/test/" + fileName);
        byte[] buf = new byte[1024 * 1024];
        int len = 0;
        while ((len = byteArrayInputStream.read(buf)) != -1) {
            fileOutputStream.write(buf , 0 , len);
            fileOutputStream.flush();
        }

        fileOutputStream.close();
        Map<String , Object> responseMap = new HashMap<>(4);
        responseMap.put("msg" , "successful");
        responseMap.put("url" , "https://192.168.1.154/img/" + fileName);
        responseMap.put("code" , 0);
        return ResponseEntity.ok(responseMap);
    }

    @GetMapping(
            path = "/node"
    )
    public ResponseEntity<Map<String , Object>> getNodeKey() {
        SurveyNode surveyNode0 = new SurveyNode();
        String orderNumber0 = UUID.randomUUID().toString();
        surveyNode0.setOrderNumber(orderNumber0);
        WilddogSyncUtils.addNode("surveys/" +  orderNumber0 , surveyNode0 , null , new CompletionListenerLogger());
        Map<String , Object> responseMap = new HashMap<>(4);
        responseMap.put("msg" , "successful");
        responseMap.put("orderNumber" , orderNumber0);
        return ResponseEntity.ok(responseMap);
    }

    @PostMapping(
            path = "/token" ,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE ,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<Map<String , Object>> getCustomToken(@RequestBody VideoConnectionDTO videoConnection) {

        if (LOG.isInfoEnabled()) {
            LOG.info("get Custom Token for Wilddog Auth , args -> {} " , videoConnection);
        }

        Map<String , Object> responseMap = new HashMap<>(4);
        responseMap.put("msg" , "successful");

        Map<String, Object> claims = new HashMap<>(4);
        claims.put("claim_0" , DateFormatUtils.format(new Date() , "yyyy-MM-dd HH:mm:ss"));
        claims.put("claim_1" , 1234556);
        responseMap.put("token" , this.wilddogAuthService.generateCustomToken(UUID.randomUUID().toString(), claims));
        return ResponseEntity.ok(responseMap);
    }
}
