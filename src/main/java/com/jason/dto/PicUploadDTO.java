package com.jason.dto;

/**
 * @author he peng
 * @create 2017/12/14 18:57
 * @see
 */
public class PicUploadDTO {

    private String content;

    public String getContent() {
        return content;
    }

    public PicUploadDTO setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        return "PicUploadDTO{" +
                "content='" + content + '\'' +
                '}';
    }
}
