package com.jason.dto;

/**
 * @author he peng
 * @create 2017/12/15 16:07
 * @see
 */
public class VideoConnectionDTO {

    private String surveyNumber;
    private String videoRoomId;

    public String getSurveyNumber() {
        return surveyNumber;
    }

    public VideoConnectionDTO setSurveyNumber(String surveyNumber) {
        this.surveyNumber = surveyNumber;
        return this;
    }

    public String getVideoRoomId() {
        return videoRoomId;
    }

    public VideoConnectionDTO setVideoRoomId(String videoRoomId) {
        this.videoRoomId = videoRoomId;
        return this;
    }

    @Override
    public String toString() {
        return "VideoConnectionDTO{" +
                "surveyNumber='" + surveyNumber + '\'' +
                ", videoRoomId='" + videoRoomId + '\'' +
                '}';
    }
}
