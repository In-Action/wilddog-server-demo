package com.jason.core.wilddog.node;

/**
 * @author he peng
 * @create 2017/12/15 14:44
 * @see
 */
public class SurveyNode {

    private String orderNumber;

    public String getOrderNumber() {
        return orderNumber;
    }

    public SurveyNode setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

}
