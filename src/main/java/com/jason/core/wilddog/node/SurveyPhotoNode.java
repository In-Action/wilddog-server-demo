package com.jason.core.wilddog.node;

/**
 * @author he peng
 * @create 2017/12/15 14:57
 * @see
 */
public class SurveyPhotoNode {

    private String url;
    private String comment;
    private Integer type;

    public String getUrl() {
        return url;
    }

    public SurveyPhotoNode setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public SurveyPhotoNode setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public SurveyPhotoNode setType(Integer type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "SurveyPhotoNode{" +
                "url='" + url + '\'' +
                ", comment='" + comment + '\'' +
                ", type=" + type +
                '}';
    }
}
