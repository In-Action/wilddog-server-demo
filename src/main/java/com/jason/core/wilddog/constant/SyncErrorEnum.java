package com.jason.core.wilddog.constant;

/**
 * @author he peng
 * @create 2017/12/18 11:24
 * @see
 */
public enum SyncErrorEnum {

    SERVER_INDICATED_OPERATION_FAILED(26001 ,
                "The server indicated that this operation failed" ,
                "服务端原因导致操作失败"),

    SERVER_BUSY_NOW(26002 ,
                "Wilddog server is busy now, please try again later" ,
                "服务器繁忙"),

    SERVER_UNAVAILABLE(26003 , "Server unavailable" , "服务不可用"),

    CLIENT_NO_PERMISSION(26101 ,
                "This client does not have permission to perform this operation" ,
                "客户端没有权限执行此操作"),

    QUOTA_LIMIT_EXCEEDED(26102 ,
                "Quota limit exceeded. Please contact support@wilddog.com" ,
                "超出套餐限制，请续费或者联系 support@wilddog.com"),

    QPS_SPEEDING(26103 , "QPS speeding, please reduce qps" ,
                "操作数据超过 5 秒 120 次的限制（ 读操作不受此限制）"),


    DATA_EXCEED_MAXIMUM_SIZE(26104,
                "Data requested exceeds the maximum size that can be accessed with a single request" ,
                "单次请求数据量过大"),



    ;

    SyncErrorEnum(int code, String message, String comment) {
        this.code = code;
        this.message = message;
        this.comment = comment;
    }

    private int code;
    private String message;
    private String comment;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "SyncErrorEnum{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
