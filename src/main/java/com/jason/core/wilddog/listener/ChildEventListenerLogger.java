package com.jason.core.wilddog.listener;

import com.wilddog.client.ChildEventListener;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.SyncError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author he peng
 * @create 2017/12/15 13:46
 * @see
 */
public class ChildEventListenerLogger implements ChildEventListener {

    private static final Logger LOG = LoggerFactory.getLogger(ChildEventListenerLogger.class);

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        if (LOG.isInfoEnabled()) {
            LOG.info("node -> {} , key -> {} , value -> {} , priority -> {} , children count -> {} " ,
                    dataSnapshot.getRef() , dataSnapshot.getKey() , dataSnapshot.getValue() ,
                    dataSnapshot.getPriority() , dataSnapshot.getChildrenCount());
        }
        childAdded(dataSnapshot ,s);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        if (LOG.isInfoEnabled()) {
            LOG.info("node -> {} , key -> {} , value -> {} , priority -> {} , children count -> {} " ,
                    dataSnapshot.getRef() , dataSnapshot.getKey() , dataSnapshot.getValue() ,
                    dataSnapshot.getPriority() , dataSnapshot.getChildrenCount());
        }
        childChanged(dataSnapshot , s);
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        if (LOG.isInfoEnabled()) {
            LOG.info("node -> {} , key -> {} , value -> {} , priority -> {} , children count -> {} " ,
                    dataSnapshot.getRef() , dataSnapshot.getKey() , dataSnapshot.getValue() ,
                    dataSnapshot.getPriority() , dataSnapshot.getChildrenCount());
        }
        childRemoved(dataSnapshot);
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        if (LOG.isInfoEnabled()) {
            LOG.info("node -> {} , key -> {} , value -> {} , priority -> {} , children count -> {} " ,
                    dataSnapshot.getRef() , dataSnapshot.getKey() , dataSnapshot.getValue() ,
                    dataSnapshot.getPriority() , dataSnapshot.getChildrenCount());
        }
        childMoved(dataSnapshot , s);
    }

    @Override
    public void onCancelled(SyncError syncError) {
        LOG.error("wilddog sync error , code -> {} , msg -> {} , details -> {} " ,
                    syncError.getErrCode() , syncError.getMessage() , syncError.getDetails());
        cancelled(syncError);
    }

    protected void childAdded(DataSnapshot dataSnapshot, String s) {}

    protected void childChanged(DataSnapshot dataSnapshot, String s) {}

    protected void childRemoved(DataSnapshot dataSnapshot) {}

    protected void childMoved(DataSnapshot dataSnapshot, String s) {}

    protected void cancelled(SyncError syncError) {}
}
