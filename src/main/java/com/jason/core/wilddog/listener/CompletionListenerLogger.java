package com.jason.core.wilddog.listener;

import com.wilddog.client.SyncError;
import com.wilddog.client.SyncReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * @author he peng
 * @create 2017/12/15 15:27
 * @see
 */
public class CompletionListenerLogger implements SyncReference.CompletionListener {

    private static final Logger LOG = LoggerFactory.getLogger(CompletionListenerLogger.class);

    @Override
    public void onComplete(SyncError syncError, SyncReference syncReference) {
        if (LOG.isInfoEnabled()) {
            if (Objects.nonNull(syncError)) {
                LOG.error("wilddog sync operation error , code -> {} , msg -> {} , details -> {} , node -> {} , key -> {} " ,
                            syncError.getErrCode() , syncError.getMessage() , syncError.getDetails() ,
                        syncReference , syncReference.getKey());
            }
            LOG.info("wilddog sync successful , node -> {} , key -> {} " , syncReference , syncReference.getKey());
        }
        internalComplete(syncError , syncReference);
    }

    protected void internalComplete(SyncError syncError, SyncReference syncReference) {}
}
