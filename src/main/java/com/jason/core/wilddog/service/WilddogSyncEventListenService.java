package com.jason.core.wilddog.service;

/**
 * @author he peng
 * @create 2017/12/15 13:31
 * @see
 */
public interface WilddogSyncEventListenService {

    void registerNodeListener(String nodeKey);
}
