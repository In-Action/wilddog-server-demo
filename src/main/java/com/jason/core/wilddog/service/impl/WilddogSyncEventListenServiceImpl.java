package com.jason.core.wilddog.service.impl;

import com.jason.config.WilddogConfiguration;
import com.jason.core.wilddog.listener.ChildEventListenerLogger;
import com.jason.core.wilddog.service.WilddogSyncEventListenService;
import com.jason.core.wilddog.util.WilddogSyncUtils;
import com.wilddog.client.ChildEventListener;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.SyncError;
import com.wilddog.client.WilddogSync;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author he peng
 * @create 2017/12/15 13:32
 * @see
 */
public class WilddogSyncEventListenServiceImpl implements WilddogSyncEventListenService {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogSyncEventListenServiceImpl.class);

    @Autowired
    private WilddogConfiguration wilddogConf;

    @Override
    public void registerNodeListener(String nodeKey) {
        if (LOG.isInfoEnabled()) {
            LOG.info("为节点 {} 注册监听" , nodeKey);
        }

        WilddogSync wilddogSync = WilddogSyncUtils.getWilddogSyncInstance(this.wilddogConf.getAppId() , com.wilddog.client.Logger.Level.DEBUG);
        wilddogSync.getReference(nodeKey).addChildEventListener(new ChildEventListenerLogger() {

            @Override
            protected void childAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            protected void childChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            protected void childRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            protected void childMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            protected void cancelled(SyncError syncError) {

            }
        });
    }
}
