package com.jason.core.wilddog.service.impl;

import com.jason.config.WilddogConfiguration;
import com.jason.core.wilddog.service.WilddogAuthService;
import com.wilddog.client.auth.CustomTokenGenerator;
import com.wilddog.client.auth.TokenOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author he peng
 * @create 2017/12/13 10:59
 * @see
 */

@Service
public class WilddogAuthServiceImpl implements WilddogAuthService {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogAuthServiceImpl.class);

    @Autowired
    private WilddogConfiguration wilddogConf;

    @Override
    public String generateCustomToken(String uId , Map<String , Object> claims) {
        TokenOptions options = new TokenOptions();
        options.setExpires(new Date(System.currentTimeMillis() + 2 * 24 * 3600 * 1000L));
        String token = CustomTokenGenerator.createCustomToken(uId, claims, this.wilddogConf.getAuthSuperToken(), options);
        return token;
    }
}
