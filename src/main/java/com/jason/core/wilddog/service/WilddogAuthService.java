package com.jason.core.wilddog.service;


import java.util.Map;

/**
 * 野狗云鉴权服务接口
 * @author he peng
 * @create 2017/12/13 10:52
 * @see
 */
public interface WilddogAuthService {

    String generateCustomToken(String uId , Map<String , Object> claims);
}
