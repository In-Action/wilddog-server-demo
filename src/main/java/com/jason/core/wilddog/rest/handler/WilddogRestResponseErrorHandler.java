package com.jason.core.wilddog.rest.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

/**
 * @author He Peng
 * @create 2017-12-23 22:41
 * @update 2017-12-23 22:41
 * @updatedesc : 更新说明
 * @see
 */
public class WilddogRestResponseErrorHandler extends DefaultResponseErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogRestResponseErrorHandler.class);

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        LOG.error("Request Wilddog Server Error {} " , response);
        super.handleError(response);
    }
}
