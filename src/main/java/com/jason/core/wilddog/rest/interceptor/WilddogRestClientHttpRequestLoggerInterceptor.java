package com.jason.core.wilddog.rest.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * @author He Peng
 * @create 2017-12-23 22:53
 * @update 2017-12-23 22:53
 * @updatedesc : 更新说明
 * @see
 */
public class WilddogRestClientHttpRequestLoggerInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogRestClientHttpRequestLoggerInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        if (LOG.isInfoEnabled()) {
            LOG.info("Request Wilddog Server ==> {} " , request);
        }
        ClientHttpResponse httpResponse = execution.execute(request, body);
        if (LOG.isInfoEnabled()) {
            LOG.info("Receive Wilddog Server Response ==> {} " , httpResponse);
        }
        return httpResponse;
    }
}
