package com.jason.config;

import com.jason.core.wilddog.util.WilddogSyncUtils;
import com.wilddog.client.Logger;
import com.wilddog.client.WilddogSync;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author he peng
 * @create 2017/12/15 13:36
 * @see
 */

@Configuration
public class BeanDefinitionConfiguration {

    @Bean
    public WilddogSync initWilddogSync(WilddogConfiguration wilddogConf) {
        return WilddogSyncUtils.getWilddogSyncInstance(wilddogConf.getAppId() , Logger.Level.DEBUG);
    }
}
