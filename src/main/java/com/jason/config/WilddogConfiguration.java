package com.jason.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author he peng
 * @create 2017/12/13 11:59
 * @see
 */

@ConfigurationProperties(prefix = "wilddog")
@Configuration
public class WilddogConfiguration {

    private String appId;
    private String videoAppId;
    private String authSuperToken;

    public String getAppId() {
        return appId;
    }

    public WilddogConfiguration setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public String getVideoAppId() {
        return videoAppId;
    }

    public WilddogConfiguration setVideoAppId(String videoAppId) {
        this.videoAppId = videoAppId;
        return this;
    }

    public String getAuthSuperToken() {
        return authSuperToken;
    }

    public WilddogConfiguration setAuthSuperToken(String authSuperToken) {
        this.authSuperToken = authSuperToken;
        return this;
    }

    @Override
    public String toString() {
        return "WilddogConfiguration{" +
                "appId='" + appId + '\'' +
                ", videoAppId='" + videoAppId + '\'' +
                ", authSuperToken='" + authSuperToken + '\'' +
                '}';
    }
}
