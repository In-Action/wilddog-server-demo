package com.jason.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * @author he peng
 * @create 2017/12/13 12:08
 * @see
 */

@SpringBootApplication
@ComponentScan(basePackages = "com.jason")
@ImportResource(locations = "classpath:spring-property.xml")
public class WilddogServerApplication {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogServerApplication.class);

    public static void main(String[] args) {

        if (LOG.isInfoEnabled()) {
            LOG.info("wild dog server app start.");
        }
        SpringApplication.run(WilddogServerApplication.class,args);

        if (LOG.isInfoEnabled()) {
            LOG.info("wild dog server app start successful.");
        }

    }
}
