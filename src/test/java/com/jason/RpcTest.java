package com.jason;

import com.alibaba.fastjson.JSON;
import com.jason.main.WilddogServerApplication;
import com.zcbl.malaka.rpc.common.Malaka;
import com.zcbl.malaka.rpc.common.url.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author he peng
 * @create 2017/12/22 19:35
 * @see
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = WilddogServerApplication.class)
public class RpcTest {

//    @Test
    public void rpcTest() throws Exception {
        Map<String , String > data = new HashMap<>(4);
        data.put("key" , "value");
        Response response = Malaka.remote("surveyOrderSyncNodeService/order/video/node").times(6).request(data).result();

    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-property.xml");
        applicationContext.start();

        Map<String , String > data = new HashMap<>(4);
        data.put("key" , "value");
        Response response = Malaka.remote("surveyOrderSyncNodeService/order/video/node").request(data).result();
    }
}
