package com.jason;

import net.sf.jmimemagic.Magic;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;

/**
 * @author he peng
 * @create 2017/12/14 17:52
 * @see
 */
public class PictureBase64EncodeTest {

    @Test
    public void fileCopy() throws Exception {
        Path path = Paths.get("D:\\Documents\\Pictures\\逗图\\earth.png");
        FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1024 * 1024);

        FileChannel fileChannel1 = FileChannel.open(
                    Paths.get("C:\\copy_earth.png") , StandardOpenOption.CREATE , StandardOpenOption.WRITE);

        fileChannel.transferTo(0 , fileChannel.size() , fileChannel1);
        System.out.println("finished");
    }

    @Test
    public void encode() throws Exception {
        Path path = Paths.get("D:\\Documents\\Pictures\\逗图\\earth.png");
        FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1024 * 1024);

        byte[] bytes = new byte[1024 * 1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int len = fileChannel.read(byteBuffer);
        while (len != -1 && len != 0) {
            System.out.println("len = " + len);
            byteBuffer.flip();
            byteBuffer.get(bytes);
            byteArrayOutputStream.write(bytes , 0 ,len);
            byteArrayOutputStream.flush();
//            byteBuffer.flip();
            len = fileChannel.read(byteBuffer);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        System.out.println("bytes size = " + byteArray.length); // 1048576
        String encodeToString = Base64.getEncoder().encodeToString(byteArray);

        System.out.println("*********" + encodeToString);
    }

    @Test
    public void encode2() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("D:\\Documents\\Pictures\\逗图\\cc9015ef76c6a7effcd93d9cf6faaf51f2de660a.jpg");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024 * 1024];
        int len = 0;
        while ((len = fileInputStream.read(bytes)) != -1) {
            byteArrayOutputStream.write(bytes , 0 ,len);
            byteArrayOutputStream.flush();
        }

        byte[] byteArray = byteArrayOutputStream.toByteArray();
        System.out.println("bytes size = " + byteArray.length); // 1728410
        String mimeType = Magic.getMagicMatch(byteArray).getMimeType();
        String mimeType1 = Magic.getMagicMatch(byteArray, false).getExtension();
        System.out.println("mimeType = " + mimeType);
        System.out.println("mimeType1 = " + mimeType1);

        String encodeToString = Base64.getEncoder().encodeToString(byteArray);

//        System.out.println("encodeToString : " + encodeToString);

        fileInputStream.close();
        fileInputStream.close();

    }
}
