package com.jason.core.wilddog;

import com.wilddog.client.auth.CustomTokenGenerator;
import com.wilddog.client.auth.IdTokenVerifier;
import com.wilddog.client.auth.TokenOptions;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author he peng
 * @create 2017/12/13 11:02
 * @see
 */
public class WilddogAuthTest {

    @Test
    public void generateCustomToken() throws Exception {
        String appId = "wd4799801019rsiebf";
        String secret = "p8RzypmDG827yhVv8ul0qZfVj4PEnMEAXeKZ9BUH";
        Map<String, Object> developerClaims = new HashMap<String, Object>();
        developerClaims.put("claims1", 112);
        developerClaims.put("claims2", true);
        developerClaims.put("claims3", "daskdjalk");
        developerClaims.put("claims4", "jhsadasjhg328y98hjfk");

        Map<String, Object> developerClaims2 = new HashMap<String, Object>();
        developerClaims2.put("aaa", 212);
        developerClaims2.put("bbb", "bbb");
        developerClaims.put("claims3", developerClaims2);
        TokenOptions options = new TokenOptions();
        options.setExpires(new Date(System.currentTimeMillis() + 7 * 24 * 3600 * 1000L));
        String token = CustomTokenGenerator.createCustomToken("some-uid", developerClaims, secret, options);

        // 野狗说的,暂时忽略这个接口
        IdTokenVerifier.VerifyResult verifyResult = IdTokenVerifier.verifyIdToken(token, appId);
        System.err.println("verify Id Token Result : " + verifyResult.isValid());
        System.err.println("Id Token : " + verifyResult.getIdToken());
        System.err.println("token : " + token);
    }
}
