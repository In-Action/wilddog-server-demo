package com.jason.core.wilddog;

import com.wilddog.client.ChildEventListener;
import com.wilddog.client.DataSnapshot;
import com.wilddog.client.SyncError;
import com.wilddog.client.SyncReference;
import com.wilddog.client.WilddogSync;
import com.wilddog.wilddogcore.WilddogApp;
import com.wilddog.wilddogcore.WilddogOptions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Objects;


/**
 * @author he peng
 * @create 2017/12/14 14:39
 * @see
 */
public class WilddogSyncDanmuDemo {

    private static final Logger LOG = LoggerFactory.getLogger(WilddogSyncDanmuDemo.class);

    public static void main(String[] args) {

        String msgPrefix = "和鹏 say :";
        String appId = "wd4799801019rsiebf";
        SyncReference rootNodeRef = init(appId, "/");
        SyncReference messageNodeRef = rootNodeRef.child("message");
        messageNodeRef.push().setValue(msgPrefix + "来自 java 端的一条弹幕$$$");
        registerChildNodeListener(messageNodeRef);
        readInputFromKeyboardPushToSync(messageNodeRef , msgPrefix);
    }

    private static SyncReference init(String appId , String path) {
        WilddogOptions options = new WilddogOptions.Builder()
                .setSyncUrl("https://" + appId + ".wilddogio.com").build();
        WilddogApp.initializeApp(options);
        return WilddogSync.getInstance().getReference(StringUtils.defaultString(path , "/"));
    }

    private static void registerChildNodeListener(SyncReference ref) {
        ref.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String time = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss:SSS");
                LOG.info("time [ {} ] - [ {} ] - [ {} ]" , time , dataSnapshot.getValue() , s);
                LOG.debug("node -> {} , key -> {} , value -> {} , priority -> {} , children count -> {} " ,
                            dataSnapshot.getRef() , dataSnapshot.getKey() , dataSnapshot.getValue() ,
                        dataSnapshot.getPriority() , dataSnapshot.getChildrenCount());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(SyncError syncError) {

            }
        });
    }

    private static void readInputFromKeyboardPushToSync(SyncReference ref , String msgPrefix) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String line = "";
            while (Objects.nonNull(line = bufferedReader.readLine())) {
                String time = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss:SSS");
                LOG.debug("time [ {} ] - [ {} ] " , time , msgPrefix + line);
                ref.push().setValue(line);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage() , e);
        } finally {
            if (Objects.nonNull(bufferedReader)) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    LOG.error(e.getMessage() , e);
                }
            }
        }
    }
}
