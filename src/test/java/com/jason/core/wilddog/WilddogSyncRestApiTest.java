package com.jason.core.wilddog;

import com.alibaba.fastjson.JSON;
import com.jason.main.WilddogServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author he peng
 * @create 2017/12/21 17:22
 * @see
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WilddogServerApplication.class)
public class WilddogSyncRestApiTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void put() throws Exception {
        String s = UUID.randomUUID().toString();
        Long uId = 666L;
        String url = "https://wd7055430119ruyynm.wilddogio.com/" + s + "/" + s + "_" + uId + ".json";
        Map<String , Object> data = new HashMap<>(4);
        data.put("username" , "小华");
        data.put("age" , 18);
        this.restTemplate.put(url , JSON.toJSONString(data));

    }



    @Test
    public void webSessionNodeTest() throws Exception {

        String webSessionNodeRootPath = "web_sessions";
        String orgCodeNode = "49bf1687-9714-482e-9202-88d386307ab3";
        Long userId = 666L;
        String webSurveyorSessionNodePath = webSessionNodeRootPath + "/" + orgCodeNode + "/" + orgCodeNode +"_" + userId;
        String url = "https://wd4799801019rsiebf.wilddogio.com/web_sessions/49bf1687-9714-482e-9202-88d386307ab3/49bf1687-9714-482e-9202-88d386307ab3_666.json";

        Map<String , Object> data = new HashMap<>(4);

        this.restTemplate.put(url , data);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<Object> objectHttpEntity = new HttpEntity<>("APP$$PHOTO$$<originalPhotoUrl>-<watermarkPhotoUrl>" , headers);
        String responseContent = this.restTemplate.postForObject(url, objectHttpEntity, String.class);
        System.out.println(responseContent);

        this.restTemplate.postForObject(
                url,
                new HttpEntity<>("SERVER$$SURVEY_ORDER_PATH$$survey_orders%2017-12-22%209556af-f63d-4e13-9a22-fdb50215c9361513922619984" , headers), String.class);
    }

    @Test
    public void surveyOrderNodeTest() throws Exception {
        String surveyNumber = UUID.randomUUID().toString();
        long timestamp = System.currentTimeMillis();
        String nodeKey = surveyNumber + timestamp;
        String dataNodeUrl = "https://wd4799801019rsiebf.wilddogio.com/survey_orders/2017-12-22/" + nodeKey + ".json";

        Map<String , Object> dataMap = new HashMap<>(7);
        dataMap.put("surveyNo" , surveyNumber);
        dataMap.put("liveSurveyorName" , "忍者神龟");
        dataMap.put("liveSurveyorPhone" , 12345678976L);
        dataMap.put("liveSurveyorStatus" , "08");
        dataMap.put("orderStatus" , "07");
        dataMap.put("reporterPhone" , 12345221321L);
        dataMap.put("reporterName" , "哆啦A梦");
        dataMap.put("reportLocation" , "XX市xx区");
        dataMap.put("insuranceCompanyName" , "中国大地财产保险公司");

        this.restTemplate.put(dataNodeUrl , dataMap);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        String videoConnectNodeUrl = "https://wd4799801019rsiebf.wilddogio.com/survey_orders/2017-12-22/" + nodeKey + "/video_connection.json";
//        this.restTemplate.put(videoConnectNodeUrl , null);
        HttpEntity<Object> objectHttpEntity = new HttpEntity<>("ping" , headers);
        String responseContent = this.restTemplate.postForObject(videoConnectNodeUrl, objectHttpEntity, String.class);
        System.out.println(responseContent);

        String videoSessionNodeUrl = "https://wd4799801019rsiebf.wilddogio.com/survey_orders/2017-12-22/" + nodeKey + "/video_session.json";
//        this.restTemplate.put(videoSessionNodeUrl , null);
        objectHttpEntity = new HttpEntity<>("SERVER$$SURVEY_ORDER_PATH$$" , headers);
        responseContent = this.restTemplate.postForObject(videoSessionNodeUrl, objectHttpEntity, String.class);
        System.out.println(responseContent);
    }

    @Test
    public void post() throws Exception {
        String url = "https://wd4799801019rsiebf.wilddogio.com/rest/users.json";
        Map<String , Object> data = new HashMap<>(4);
        data.put("username" , "丹尼");
        data.put("age" , 22);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<Object> objectHttpEntity = new HttpEntity<>(data , headers);
        String responseContent = this.restTemplate.postForObject(url, objectHttpEntity, String.class);
        System.out.println(responseContent);
    }

    @Test
    public void delete() throws Exception {
        String url = "https://wd7055430119ruyynm.wilddogio.com/survey_orders.json";
        this.restTemplate.delete(url);
    }
}
