package com.jason.core.wilddog;

import com.jason.core.wilddog.node.SurveyNode;
import com.wilddog.client.SyncError;
import com.wilddog.client.SyncReference;
import com.wilddog.client.WilddogSync;
import com.wilddog.wilddogcore.WilddogApp;
import com.wilddog.wilddogcore.WilddogOptions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @author he peng
 * @create 2017/12/14 14:14
 * @see
 */
public class WilddogSyncCrudTest {

    static final Logger LOG = LoggerFactory.getLogger(WilddogSyncCrudTest.class);

    public static void main(String[] args) {
        String appId = "wd4799801019rsiebf";
        WilddogOptions options = new WilddogOptions.Builder()
                .setSyncUrl("https://" + appId + ".wilddogio.com").build();
        WilddogApp wilddogApp = WilddogApp.initializeApp(options);
        SyncReference rootRef = WilddogSync.getInstance().getReference("/");
        SyncReference qqFriendsRef = rootRef.child("qq_friends");

        addNode(qqFriendsRef);
        addNodeWithPriority(qqFriendsRef);
        appendNode(qqFriendsRef);
        updateNode(qqFriendsRef);
        removeNode(qqFriendsRef);

        addTakePicNode(rootRef , false);

//        addTakeObjNode(rootRef);

        bizTest(rootRef);
        getNode(rootRef);
    }

    public static void getNode(SyncReference rootRef) {
        SyncReference ref = rootRef.child("surveys/4c60a9e0-7736-4c8e-9303-f67d95aa4430");
    }

    private static void bizTest(SyncReference rootRef) {
        SurveyNode surveyNode0 = new SurveyNode();
        String orderNumber0 = UUID.randomUUID().toString();
        surveyNode0.setOrderNumber(orderNumber0);
        SyncReference surveysRef = rootRef.child("surveys");
        surveysRef.child(orderNumber0).setValue(surveyNode0 ,
                (SyncError syncError, SyncReference syncReference) -> {
                    if (Objects.nonNull(syncError)) {
                        LOG.debug("Sync Error , code {} , message {} , Sync Reference {} " ,
                                syncError.getErrCode() , syncError.getMessage() , syncReference);
                    }
        });

        LOG.info("add node ==> {} " , surveyNode0);

        SurveyNode surveyNode1 = new SurveyNode();
        String orderNumber1 = UUID.randomUUID().toString();
        surveyNode1.setOrderNumber(orderNumber1);
        surveysRef.child(orderNumber1).setValue(surveyNode1 , (SyncError syncError, SyncReference syncReference) -> {
            if (Objects.nonNull(syncError)) {
                LOG.debug("Sync Error , code {} , message {} , Sync Reference {} " ,
                        syncError.getErrCode() , syncError.getMessage() , syncReference);
            }
        });

        LOG.info("add node ==> {} " , surveyNode1);
    }

    private static void addTakeObjNode(SyncReference rootRef) {
        User user = new User();
        user.setAge(12).setName("hahahh");
        rootRef.setValue(user);
        rootRef.child("*^&%^&^%^&").setValue(user);
    }


    private static void removeNode(SyncReference qqFriendsRef) {
        qqFriendsRef.child("2581800887").removeValue((SyncError syncError , SyncReference syncRef) -> {
            if (Objects.nonNull(syncError)) {
                LOG.debug("remove Node 2581800887 Failed Sync Error , code {} , message {} , Sync Reference {} " ,
                        syncError.getErrCode() , syncError.getMessage() , syncRef);
            } else {
                LOG.debug("remove Node 2581800887 Successful Sync Reference {} " , syncRef);
            }
        });
    }

    private static void updateNode(SyncReference qqFriendsRef) {
        Map<String , Object> nodeMap = new HashMap<>(4);
        nodeMap.put("slogan" , "我要从黎明奔跑到黄昏，去看一看天边的日落。就像是能够逃离这城市,却留不住六月的某一个早晨。");
        SyncReference ref = qqFriendsRef.child("2581800887");
        ref.updateChildren(nodeMap , (SyncError syncError , SyncReference syncRef) -> {
            if (Objects.nonNull(syncError)) {
                LOG.debug("update Node 2581800887 Failed Sync Error , code {} , message {} , Sync Reference {} " ,
                        syncError.getErrCode() , syncError.getMessage() , syncRef);
            } else {
                LOG.debug("update Node 2581800887 Successful Sync Reference {} " , syncRef);
            }
        });
    }


    private static void addTakePicNode(SyncReference rootRef , boolean isTakePic) {
        rootRef.child("takePic").setValue(isTakePic);
    }

    private static void appendNode(SyncReference qqFriendsRef) {
        Map<String , Object> nodeMap = new HashMap<>(4);
        nodeMap.put("nickname" , "Stephanie");
        nodeMap.put("email" , "2581800887@qq.com");
        nodeMap.put("gender" , "female");
        nodeMap.put("age" , "24");
        nodeMap.put("constellation" , "双鱼座");
        nodeMap.put("slogan" , "");

        SyncReference ref = qqFriendsRef.child("2581800887");
        ref.setValue(nodeMap);


        ref.child("phone").setValue("184******75" ,
                    (SyncError syncError , SyncReference syncRef) -> {
                        if (Objects.nonNull(syncError)) {
                            LOG.debug("Push Node Failed Sync Error , code {} , message {} , Sync Reference {} " ,
                                    syncError.getErrCode() , syncError.getMessage() , syncRef);
                        } else {
                            LOG.debug("Push Node Successful Sync Reference {} " , syncRef);
                        }
                    });

        LOG.info("push node ==> {} " , nodeMap);
    }

    private static void addNodeWithPriority(SyncReference ref) {
        Map<String , Object> nodeMap = new HashMap<>(4);
        nodeMap.put("nickname" , "沧海");
        nodeMap.put("email" , "460332339@qq.com");
        nodeMap.put("gender" , "male");
        nodeMap.put("age" , "5");
        nodeMap.put("constellation" , "");
        nodeMap.put("slogan" , "初心勿忘");
        ref.child("460332339").setValue(nodeMap, 666,
                (SyncError syncError, SyncReference syncReference) -> {
                    if (Objects.nonNull(syncError)) {
                        LOG.debug("Sync Error , code {} , message {} , Sync Reference {} " ,
                                syncError.getErrCode() , syncError.getMessage() , syncReference);
                    }
                });

        LOG.info("add node with priority ==> {} " , nodeMap);
    }

    private static void addNode(SyncReference ref) {
        Map<String , Object> nodeMap = new HashMap<>(4);
        nodeMap.put("nickname" , "Panama");
        nodeMap.put("email" , "liu295740949@vip.qq.com");
        nodeMap.put("gender" , "male");
        nodeMap.put("age" , "1");
        nodeMap.put("constellation" , "摩羯座");
        nodeMap.put("slogan" , "我一直很清醒，清醒的看着自己的沉沦");
        ref.child("295740949").setValue(nodeMap);

        LOG.info("add node ==> {} " , nodeMap);
    }
}
